
minetest.register_craft({
    output = 'han:到',
    recipe = {
            {''      , ''      , ''      }
            {'han:至', ''      , 'han:刂'}
            {''      , ''      , ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:他',
    recipe = {
            {''      , ''      , ''      }
            {'han:亻', ''      , 'han:也'}
            {''      , ''      , ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:们',
    recipe = {
            {'han:亻', ''      , 'han:门'}
        }
    }
})

minetest.register_craft({
    output = 'han:們',
    recipe = {
            {''      , ''      , ''      }
            {'han:亻', ''      , 'han:門'}
            {''      , ''      , ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:個',
    recipe = {
            {''      , ''      , ''      }
            {'han:亻', ''      , 'han:固'}
            {''      , ''      , ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:地',
    recipe = {
            {''      , ''      , ''      }
            {'han:土', ''      , 'han:也'}
            {''      , ''      , ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:得',
    recipe = {
            {''      , ''      , ''      }
            {'han:彳', ''      , 'han:㝵'}
            {''      , ''      , ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:的',
    recipe = {
            {''      , ''      , ''      }
            {'han:白', ''      , 'han:勺'}
            {''      , ''      , ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:那',
    recipe = {
            {''      , ''      , ''      }
            {'han:𭃂', ''      , 'han:阝'}
            {''      , ''      , ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:於',
    recipe = {
            {''      , ''      , ''      }
            {'han:方', ''      , 'han:仒'}
            {''      , ''      , ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:和',
    recipe = {
            {''      , ''      , ''      }
            {'han:禾', ''      , 'han:口'}
            {''      , ''      , ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:能',
    recipe = {
            {''      , ''      , ''      }
            {'han:䏍', ''      , 'han:𫧇'}
            {''      , ''      , ''      }
        }
    }
})
