
minetest.register_craft({
    output = 'han:人',
    recipe = {
            {'han:丿', ''      , ''      }
            {''      , ''      , ''      }
            {''      , ''      , 'han:㇏'}
        }
    }
})

minetest.register_craft({
    output = 'han:大',
    recipe = {
            {'han:𠂇', ''      , ''      }
            {''      , ''      , ''      }
            {''      , ''      , 'han:㇏'}
        }
    }
})

minetest.register_craft({
    output = 'han:不',
    recipe = {
            {'han:丆', ''      , ''      }
            {''      , ''      , ''      }
            {''      , ''      , 'han:卜'}
        }
    }
})

minetest.register_craft({
    output = 'han:有',
    recipe = {
            {'han:𠂇', ''      , ''      }
            {''      , ''      , ''      }
            {''      , ''      , 'han:月'}
        }
    }
})

minetest.register_craft({
    output = 'han:於',
    recipe = {
            {'han:𭤨', ''      , ''      }
            {''      , ''      , ''      }
            {''      , ''      , 'han:⺀'}
        }
    }
})
