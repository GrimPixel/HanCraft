
minetest.register_craft({
    output = 'han:于',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:丁', ''      }
            {''      , ''      , 'han:一'}
        }
    }
})

minetest.register_craft({
    output = 'han:大',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:一', ''      }
            {''      , ''      , 'han:人'}
        }
    }
})

minetest.register_craft({
    output = 'han:子',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:了', ''      }
            {''      , ''      , 'han:一'}
        }
    }
})

minetest.register_craft({
    output = 'han:中',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:口', ''      }
            {''      , ''      , 'han:丨'}
        }
    }
})

minetest.register_craft({
    output = 'han:也',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:乜', ''      }
            {''      , ''      , 'han:丨'}
        }
    }
})

minetest.register_craft({
    output = 'han:年',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:午', ''      }
            {''      , ''      , 'han:丄'}
        }
    }
})

minetest.register_craft({
    output = 'han:生',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:𠂉', ''      }
            {''      , ''      , 'han:土'}
        }
    }
})

minetest.register_craft({
    output = 'han:来',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:未', ''      }
            {''      , ''      , 'han:丷'}
        }
    }
})

minetest.register_craft({
    output = 'han:国',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:囗', ''      }
            {''      , ''      , 'han:玉'}
        }
    }
})

minetest.register_craft({
    output = 'han:國',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:囗', ''      }
            {''      , ''      , 'han:或'}
        }
    }
})

minetest.register_craft({
    output = 'han:来',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:木', ''      }
            {''      , ''      , 'han:䒑'}
        }
    }
})

minetest.register_craft({
    output = 'han:來',
    recipe = {
            {''      , ''      , ''      }
            {''      , 'han:木', ''      }
            {''      , ''      , 'han:从'}
        }
    }
})
