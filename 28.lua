
minetest.register_craft({
    output = 'han:于',
    recipe = {
            {''      , 'han:一', ''      }
            {''      , ''      , ''      }
            {''      , 'han:𬺰', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:上',
    recipe = {
            {''      , 'han:⺊', ''      }
            {''      , ''      , ''      }
            {''      , 'han:一', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:下',
    recipe = {
            {''      , 'han:一', ''      }
            {''      , ''      , ''      }
            {''      , 'han:卜', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:生',
    recipe = {
            {''      , 'han:牛', ''      }
            {''      , ''      , ''      }
            {''      , 'han:一', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:而',
    recipe = {
            {''      , 'han:一', ''      }
            {''      , ''      , ''      }
            {''      , 'han:𦓐', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:个',
    recipe = {
            {''      , 'han:人', ''      }
            {''      , ''      , ''      }
            {''      , 'han:丨', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:自',
    recipe = {
            {''      , 'han:丿', ''      }
            {''      , ''      , ''      }
            {''      , 'han:目', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:而',
    recipe = {
            {''      , 'han:丆', ''      }
            {''      , ''      , ''      }
            {''      , 'han:𦉫', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:年',
    recipe = {
            {''      , 'han:𠂉', ''      }
            {''      , ''      , ''      }
            {''      , 'han:㐄', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:會',
    recipe = {
            {''      , 'han:亼', ''      }
            {''      , ''      , ''      }
            {''      , 'han:𭥴', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:会',
    recipe = {
            {''      , 'han:人', ''      }
            {''      , ''      , ''      }
            {''      , 'han:云', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:著',
    recipe = {
            {''      , 'han:艹', ''      }
            {''      , ''      , ''      }
            {''      , 'han:者', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:出',
    recipe = {
            {''      , 'han:山', ''      }
            {''      , ''      , ''      }
            {''      , 'han:山', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:会',
    recipe = {
            {''      , 'han:亼', ''      }
            {''      , ''      , ''      }
            {''      , 'han:𠫔', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:要',
    recipe = {
            {''      , 'han:覀', ''      }
            {''      , ''      , ''      }
            {''      , 'han:女', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:是',
    recipe = {
            {''      , 'han:日', ''      }
            {''      , ''      , ''      }
            {''      , 'han:𤴓', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:髮',
    recipe = {
            {''      , 'han:髟', ''      }
            {''      , ''      , ''      }
            {''      , 'han:犮', ''      }
        }
    }
})

minetest.register_craft({
    output = 'han:發',
    recipe = {
            {''      , 'han:癶', ''      }
            {''      , ''      , ''      }
            {''      , 'han:𭚧', ''      }
        }
    }
})
